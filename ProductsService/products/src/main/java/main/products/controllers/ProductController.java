package main.products.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import main.products.services.IProductsService;

import data.commons.models.Product;

@RestController
public class ProductController {

    @Autowired
    private IProductsService repo;

    @GetMapping("/productslist")
    public List<Product> GetProductsList() {
        return repo.findAll();
    }

    @GetMapping("/product/{id}")
    public Product GetProduct(@PathVariable Long id) {
        return repo.findById(id);
    }

    @PostMapping("/add")
    @ResponseStatus(HttpStatus.CREATED)
    public Product add(@RequestBody Product data) {
        return repo.save(data);
    }

    @PutMapping("/edit/{id}")
    @ResponseStatus(HttpStatus.CREATED)
    public Product edit(@PathVariable Long id, @RequestBody Product data) {
        Product pro = repo.findById(id);

        pro.setName(data.getName());
        pro.setPrice(data.getPrice());

        return repo.save(pro);
    }

    @DeleteMapping("/delete/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable Long id) {
        repo.deleteById(id);
    }

}