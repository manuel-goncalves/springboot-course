package main.products.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import data.commons.models.Product;
import main.products.server.IProduct;

@Service
public class ProductsService implements IProductsService {

    @Autowired
    private IProduct repo;

    @Override
    @Transactional(readOnly = true)
    public List<Product> findAll() {
        return (List<Product>) repo.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Product findById(long id) {
        return repo.findById(id).orElse(null);
    }

    @Override
    @Transactional
    public Product save(Product pro) {
        return repo.save(pro);
    }

    @Override
    @Transactional
    public void deleteById(Long id) {
        repo.deleteById(id);
    }

}