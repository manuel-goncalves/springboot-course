package main.products.services;

import java.util.List;

import data.commons.models.Product;

public interface IProductsService {

    public List<Product> findAll();

    public Product findById(long id);

    public Product save(Product pro);

    public void deleteById(Long id);
}