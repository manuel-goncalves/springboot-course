package main.products.server;

import org.springframework.data.jpa.repository.JpaRepository;

import data.commons.models.Product;

public interface IProduct extends JpaRepository<Product, Long> {

}