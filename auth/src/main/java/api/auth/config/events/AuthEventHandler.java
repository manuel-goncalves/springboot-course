package api.auth.config.events;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationEventPublisher;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import api.auth.services.IUserService;
import api.usercommons.models.User;
import feign.FeignException;

/**
 * AuthEventHandler
 */
@Component
public class AuthEventHandler implements AuthenticationEventPublisher {

    private Logger log = LoggerFactory.getLogger(AuthEventHandler.class);

    @Autowired
    private IUserService repo;

    @Override
    public void publishAuthenticationSuccess(Authentication authentication) {
        UserDetails user = (UserDetails) authentication.getPrincipal();
        String msg = "Success login: " + user.getUsername();
        System.out.println(msg);
        log.info(msg);

        User u = repo.getByUsername(authentication.getName());

        if (u.getTries() != null && u.getTries() > 0) {
            u.setTries(0);
            repo.update(u, u.getId());
        }

    }

    @Override
    public void publishAuthenticationFailure(AuthenticationException exception, Authentication authentication) {
        String msg = "Faild to log in: " + exception.getMessage();
        log.error(msg);
        System.out.println(msg);
        try {

            User user = repo.getByUsername(authentication.getName());
            if (user.getTries() == null) {
                user.setTries(0);
            }
            log.info("Tries left: " + (3 - user.getTries()));
            user.setTries(user.getTries() + 1);
            log.info("Tries left: " + (3 - user.getTries()));

            if (user.getTries() >= 3) {
                log.error(String.format("the user %s has been disabled for max tries", user.getUsername()));
                user.setEnabled(false);
            }

            repo.update(user, user.getId());

        } catch (FeignException e) {
            log.error(String.format("the user %s does not exist in the system", authentication.getName()));
        }
    }

}