package api.auth.clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import api.usercommons.models.User;

/**
 * IUserFeignClient
 */
@FeignClient(name = "users-service")
public interface IUserFeignClient {

    @GetMapping("/users/search/getbyusername")
    public User getByUsername(@RequestParam String username);

    @PutMapping("/users/{id}")
    public User update(@RequestBody User u, @PathVariable Long id);
}