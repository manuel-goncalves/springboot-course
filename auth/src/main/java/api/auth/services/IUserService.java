package api.auth.services;

import api.usercommons.models.User;

/**
 * IUserService
 */
public interface IUserService {

    public User getByUsername(String username);

    public User update(User u, Long id);
}