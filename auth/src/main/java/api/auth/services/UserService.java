package api.auth.services;

import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import api.auth.clients.IUserFeignClient;
import api.usercommons.models.User;
import feign.FeignException;

/**
 * UserService
 */
@Service
public class UserService implements IUserService, UserDetailsService {

    private Logger log = LoggerFactory.getLogger(UserService.class);

    @Autowired
    private IUserFeignClient client;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        try {

            User user = client.getByUsername(username);

            List<GrantedAuthority> authorities = user.getRoles().stream()
                    .map(a -> new SimpleGrantedAuthority(a.getName()))
                    .peek(authority -> log.info("Role: " + authority.getAuthority())).collect(Collectors.toList());

            log.info("User: " + username);

            return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(),
                    user.getEnabled(), true, true, true, authorities);
        } catch (FeignException e) {
            log.error("User not found '" + username + "' ");
            throw new UsernameNotFoundException("User not found '" + username + "' ");
        }
    }

    @Override
    public User getByUsername(String username) {

        return client.getByUsername(username);
    }

    @Override
    public User update(User u, Long id) {
        client.update(u, id);
        return null;
    }

}