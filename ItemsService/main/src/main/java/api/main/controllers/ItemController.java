package api.main.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RestController;

import api.main.models.Item;
import data.commons.models.Product;
import api.main.services.IItemService;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * ItemController
 */
@RestController
public class ItemController {

    @Autowired
    @Qualifier("serviceFeign")
    private IItemService repo;

    @GetMapping("/list")
    public List<Item> getAll() {
        return repo.findAll();
    }

    @GetMapping("/product/{id}/{quantity}")
    public Item getOne(@PathVariable Long id, @PathVariable Integer quantity) {
        return repo.findById(id, quantity);
    }

    @PostMapping("/add")
    @ResponseStatus(HttpStatus.CREATED)
    public Product add(@RequestBody Product data) {
        return repo.save(data);
    }

    @PutMapping("/edit/{id}")
    @ResponseStatus(HttpStatus.CREATED)
    public Product edit(@RequestBody Product data, @PathVariable Long id) {

        return repo.edit(data, id);
    }

    @DeleteMapping("/delete/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable Long id) {

        repo.delete(id);
    }

}