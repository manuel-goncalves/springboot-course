package api.main.clients;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import data.commons.models.Product;

/**
 * IRestProductClient
 */
@FeignClient(name = "products-service")
public interface IRestProductClient {

    @GetMapping("/productslist")
    public List<Product> getProducts();

    @GetMapping("/product/{id}")
    public Product GetProduct(@PathVariable Long id);

    @PostMapping("/add")
    public Product add(@RequestBody Product data);

    @PutMapping("/edit/{id}")
    public Product edit(@RequestBody Product data, @PathVariable Long id);

    @DeleteMapping("/delete/{id}")
    public Product delete(@PathVariable Long id);
}