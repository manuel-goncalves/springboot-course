package api.main.services;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import api.main.models.Item;
import data.commons.models.Product;

/**
 * ItemService
 */
@Service
public class ItemService implements IItemService {
    @Autowired
    private RestTemplate restClient;

    @Override
    public List<Item> findAll() {
        List<Product> products = Arrays
                .asList(restClient.getForObject("http://products-service/productslist", Product[].class));
        return products.stream().map(p -> new Item(p, 1)).collect(Collectors.toList());
    }

    @Override
    public Item findById(Long id, Integer quantity) {
        Map<String, String> pathVars = new HashMap<String, String>();
        pathVars.put("id", id.toString());
        Product product = restClient.getForObject("http://products-service/product/{id}", Product.class, pathVars);
        return new Item(product, quantity);
    }

    @Override
    public Product save(Product data) {
        HttpEntity<Product> body = new HttpEntity<Product>(data);
        ResponseEntity<Product> resp = restClient.exchange("http://products-service/add", HttpMethod.POST, body,
                Product.class);
        return resp.getBody();
    }

    @Override
    public void delete(Long id) {
        Map<String, String> pathVars = new HashMap<String, String>();
        pathVars.put("id", id.toString());
        restClient.delete("http://products-service/delete/{id}", pathVars);
    }

    @Override
    public Product edit(Product data, Long id) {
        HttpEntity<Product> body = new HttpEntity<Product>(data);
        Map<String, String> pathVars = new HashMap<String, String>();
        pathVars.put("id", id.toString());
        ResponseEntity<Product> resp = restClient.exchange("http://products-service/edit/{id}", HttpMethod.PUT, body,
                Product.class, pathVars);
        return resp.getBody();
    }

}