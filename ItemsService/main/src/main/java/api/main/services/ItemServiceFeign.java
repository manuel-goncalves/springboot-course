package api.main.services;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import api.main.clients.IRestProductClient;
import api.main.models.Item;
import data.commons.models.Product;

/**
 * ItemServiceFeign
 */
@Service("serviceFeign")
public class ItemServiceFeign implements IItemService {
    @Autowired
    private IRestProductClient repo;

    @Override
    public List<Item> findAll() {

        return repo.getProducts().stream().map(p -> new Item(p, 1)).collect(Collectors.toList());
    }

    @Override
    public Item findById(Long id, Integer quantity) {
        return new Item(repo.GetProduct(id), quantity);
    }

    @Override
    public Product save(Product data) {
        return repo.add(data);
    }

    @Override
    public void delete(Long id) {
        repo.delete(id);
    }

    @Override
    public Product edit(Product data, Long id) {
        return repo.edit(data, id);
    }

}