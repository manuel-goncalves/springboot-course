package api.main.services;

import java.util.List;

import api.main.models.Item;
import data.commons.models.Product;

/**
 * ItemService
 */
public interface IItemService {

    public List<Item> findAll();

    public Item findById(Long id, Integer quantity);

    public Product save(Product data);

    public void delete(Long id);

    public Product edit(Product data, Long id);
}