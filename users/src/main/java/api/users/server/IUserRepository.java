package api.users.server;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import api.usercommons.models.User;

/**
 * IUserRepository
 */
@RepositoryRestResource(path = "users") // import and add CRUD end points
public interface IUserRepository extends PagingAndSortingRepository<User, Long> {

    @RestResource(path = "getbyusername") // re write function name
    public User findByUsername(String username);

    /*
     * @Query("select u from User u where u.username=?1 or u.email=?1") // Custom
     * queries public User getByUserNameOrEmail(String username);
     */
}