INSERT INTO users (username, password, name, lastname, email, enabled) VALUES ('don','$2a$10$k0G2XuoKaXpFdksjfCBI5OOhBLogWqiArPKMuAWlXUt9QEfyUU58q','don','doe','test1@cl.cl', true);
INSERT INTO users (username, password, name, lastname, email, enabled) VALUES ('ki','$2a$10$/J4SSNCe3.zGdS7FyyqmHOauj3PpQagK44Jv5ZAMySDiu8LVnnXbG','ki','doe','test2@cl.cl', true);
INSERT INTO users (username, password, name, lastname, email, enabled) VALUES ('jo','$2a$10$4kj83m1F4MoArMhbdedKUOYG8tAroGppXZZdce8iUBE6JZ8jdRXjS','jo','doe','test3@cl.cl', true);
INSERT INTO users (username, password, name, lastname, email, enabled) VALUES ('te','$2a$10$NavP4Zb7c6TmvwRuL1E84uMiwEQIqlpY5l35M9meCQn24aOMhOYmK','te','doe','test4@cl.cl', true);

INSERT INTO roles (name) VALUES ('ROLE_ADMIN');
INSERT INTO roles (name) VALUES ('ROLE_USER');

INSERT INTO users_roles (user_id, roles_id) VALUES (1,1);
INSERT INTO users_roles (user_id, roles_id) VALUES (1,2);
INSERT INTO users_roles (user_id, roles_id) VALUES (2,2);
INSERT INTO users_roles (user_id, roles_id) VALUES (3,2);
INSERT INTO users_roles (user_id, roles_id) VALUES (4,2);
